# 基于树莓派的智能垃圾分拣系统


#### 介绍
>基于树莓派4b的智能垃圾分拣系统，涉及pytorch，pyqt5。
>
>为第七届工训赛设计，获江苏省特等奖，第五名无缘国赛。
>
>也可以当作毕设。
>
>Bilibili演示视频:https://www.bilibili.com/video/BV1kN411o73u


#### 联系方式
>交流群群号:789169244
>
>我(何天骅)QQ号:3177556879


#### 开发人员
>软件部分:南京工程学院 自动化学院 机器人工程 20级大一 何天骅（主要开发人员） || 南京工程学院 20级研一 岳骏 (副开发人员)
>
>机械结构:南京工程学院 工业中心 20级大一 张毅（主要开发人员） || 南京工程学院 机械工程学院 20级大一 徐鸣远（副开发人员）


#### 文件组成

>##### data_set文件夹
>>###### train文件夹
>>>模型训练集
>>###### val文件夹
>>>模型验证集

>##### logs文件夹
>>用于存放程序运行时产生的日志，如果被删除了需要手动创建一个

>##### photo文件夹
>>用于存放程序运行时拍摄的照片，如果被删除了需要手动创建一个

>##### tools文件夹
>>###### model.py
>>>本质上是mobilenet_v2，用于训练时调用
>>###### predict.py
>>>测试模型用的程序
>>###### train.py
>>>训练模型用的程序

>##### weight文件夹
>>###### class_indices.json
>>>训练模型时自动生成的词典

>>###### xxx.pth
>>>训练好的模型

>##### alarm.py
>>控制蜂鸣器的函数

>##### getpic.py
>>拍摄训练图片用的程序

>##### main.py
>>垃圾分拣流程，注意:单独运行这个文件没有UI

>##### motocontrol.py
>>控制舵机的函数

>##### sensor.py
>>读取超声波传感器的函数

>##### ui_g.py
>>UI文件的图形部分，是ui文件用PYUIC转换然后修改过的
>
>>如果你重新生成了此文件，把PYQT5自动生成的代码即最后一行删掉，然后自己加上必要的库

>##### ui_l.py
>>UI文件的逻辑部分

>##### ui.ui
>>UI原文件

#### 电气原理图
![电气原理图](https://images.gitee.com/uploads/images/2021/0501/131846_afd4c5d9_8347966.png "yuanlitu.png")

#### 零件表
>1. 树莓派4b (用的4g运存版,2g应该也行)
>2. 扩展版 (YwRobot家的,35rmb,不是广告)
>3. 舵机 x 2 (我们用的机构里要两个舵机,由于一个是360度一个是180度,所以代码写的有些奇怪)
>4. 超声波传感器 x 4 (HC-SRO4,四针的) (建议不要用超声波,稳定性太差)
>5. 显示屏 (至少要1024x600的分辨率,否则程序界面要大改)
>6. 开关 x 6 (三个电路三个控制) (放这么多开关是老师的主意,后续来看确实有这个必要) (如果没有开关,则需要把空闲模式关掉)

#### 相关图片
>##### 大致走线图
>>![大致走线图](https://images.gitee.com/uploads/images/2021/0503/192743_9ada2d26_8347966.png "3.png")
>##### 工训赛社区赛现场
>>![工训赛社区赛现场](https://images.gitee.com/uploads/images/2021/0503/192651_54492313_8347966.png "2.png")
>##### 垃圾桶外观图
>>![垃圾桶外观图](https://images.gitee.com/uploads/images/2021/0503/192506_8f03823e_8347966.png "1.png")
>##### 校内媒体图片
>>![校内媒体图片](https://images.gitee.com/uploads/images/2021/0503/192556_7e0475dc_8347966.jpeg "mmexport1620040617070.jpg")
>##### 电视台采访图片
>>![电视台采访图片](https://images.gitee.com/uploads/images/2021/0503/192417_5917d487_8347966.png "9ROI9K2PJHH_04}%U$THDLW(1).png")
